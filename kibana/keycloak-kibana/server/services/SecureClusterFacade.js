import _ from 'lodash';
import ClusterDecorator from './ClusterDecorator';
import SavedObjectAction from './SavedObjectAction';
import Boom from 'boom';

export class SecureClusterFacade
{

  constructor( { cluster, authRules } )
  {
    this._cluster = cluster;
    this._authRules = authRules;
  }

  async callWithRequest ( req = {}, endpoint, clientParams = {}, options = {} )
  {
    const cluster = new ClusterDecorator( this._cluster );
    console.log( req.url.path )
    req[ "path" ] = req.url.path
    const action = new SavedObjectAction( {
      request: req,
      principal: "83e3a94a-e1b2-4c61-8ef6-126296b2d51e",
      cluster,
      clusterRequest: {
        endpoint,
        clientParams,
        options
      }
    } );
    console.log( action )
    console.log( this._authRules )
    const rule = _.find( this._authRules, rule => rule.matches( action ) );
    if ( rule )
    {
      return await rule.process( cluster, action );
    }
    console.warn( `${req.method} ${req.path} is not authorized.` );
    throw Boom.forbidden( 'The user is not authorized to perform this operation.' );
  }
}

export const secureSavedObjects = ( server, authRules ) =>
{
  const { savedObjects } = server;
  savedObjects.setScopedSavedObjectsClientFactory( ( { request } ) =>
  {
    const secureCluster = new SecureClusterFacade( {
      cluster: server.plugins.elasticsearch.getCluster( 'admin' ),
      authRules
    } );
    const secureCallCluster = ( ...args ) => secureCluster.callWithRequest( request, ...args );
    const secureRepository = savedObjects.getSavedObjectsRepository( secureCallCluster );
    return new savedObjects.SavedObjectsClient( secureRepository );
  } );
};
