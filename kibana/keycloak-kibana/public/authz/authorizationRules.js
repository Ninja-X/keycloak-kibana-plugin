import Roles from './constants/Roles';
import _ from 'lodash';

const hasRole = ( ...roles ) => principal => _.intersection( roles, principal.scope ).length > 0;

const hasId = id => resource => resource === id;
const pathEndsWith = pathSuffix => url => url.pathname.endsWith( pathSuffix );
const hashStartsWith = hashPrefix => url => url.hash.startsWith( hashPrefix );
const isKibanaApp = pathEndsWith( '/app/kibana' );

const whether = ( ...conditions ) => url => conditions.every( condition => condition( url ) );

const authorizationRules = {
  navLinks: [
    { resource: hasId( 'kibana:discover' ), principal: true },
    { resource: hasId( 'kibana:visualize' ), principal: true },
    { resource: hasId( 'kibana:dashboard' ), principal: true },
    { resource: hasId( 'canvas' ), principal: true },
    { resource: hasId( 'maps' ), principal: true },
    { resource: hasId( 'ml' ), principal: true },
    { resource: hasId( 'infra:home' ), principal: true },
    { resource: hasId( 'infra:logs' ), principal: true },
    { resource: hasId( 'apm' ), principal: true },
    { resource: hasId( 'uptime' ), principal: true },
    { resource: hasId( 'siem' ), principal: true },
    { resource: hasId( 'kibana:dev_tools' ), principal: true },
    { resource: hasId( 'monitoring' ), principal: true },
    { resource: hasId( 'kibana:management' ), principal: true },
    { resource: hasId( 'code' ), principal: true },
    { resource: hasId( 'graph' ), principal: true }
  ],
  routes: [
    { resource: whether( isKibanaApp, hashStartsWith( '#/home' ) ), principal: () => true },
    { resource: whether( isKibanaApp, hashStartsWith( '#/discover' ) ), principal: true },
    {
      resource: whether( isKibanaApp, hashStartsWith( '#/dashboard' ) ),
      principal: true
    },
    { resource: whether( isKibanaApp, hashStartsWith( '#/visualize/new' ) ), principal: true },
    { resource: whether( isKibanaApp, hashStartsWith( '#/visualize/edit' ) ), principal: true },
    { resource: whether( isKibanaApp, hashStartsWith( '#/visualize/create' ) ), principal: true },
    { resource: whether( isKibanaApp, hashStartsWith( '#/visualize' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/canvas' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/maps' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/ml' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/infra' ), hashStartsWith( '#/home' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/infra' ), hashStartsWith( '#/logs' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/apm' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/uptime' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/siem' ) ), principal: true },
    { resource: whether( isKibanaApp, hashStartsWith( '#/dev_tools' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/monitoring' ) ), principal: true },
    { resource: whether( pathEndsWith( '#/management' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/code' ) ), principal: true },
    { resource: whether( pathEndsWith( '/app/graph' ) ), principal: true },
  ],
  allowMissingNavLinks: true,
  allowMissingRoutes: true
};

export default authorizationRules;
